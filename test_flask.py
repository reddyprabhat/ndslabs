from flask import Flask
from flask import render_template

app = Flask(__name__)

@app.route('/maps/')
@app.route('/maps/<name>')
def maps(name=None):
    return render_template('maps.html', name=name)


if __name__ == '__main__':
    app.run()
