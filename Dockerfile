# Matias Carrasco Kind
#
# Flask/Oracle/Eups
#

FROM ubuntu:14.04
MAINTAINER Matias Carrasco Kind <mgckind@gmail.com>

ENV HOME /root
ENV SHELL /bin/bash

RUN apt-get update && apt-get -y upgrade
RUN apt-get install -y git nano git curl nano wget dialog net-tools build-essential vim unzip libaio1
RUN apt-get install -y python
RUN apt-get install -y  python-pip python-dev build-essential
RUN apt-get install -y python-numpy python-matplotlib python-scipy

RUN pip install --upgrade pip

RUN mkdir -p /test
WORKDIR /test
RUN cd /test


RUN ls
#Oracle client
RUN curl -O http://deslogin.cosmology.illinois.edu/~mcarras2/install_oracleclient.py
RUN python install_oracleclient.py
RUN rm /bin/sh && ln -s /bin/bash /bin/sh
RUN ldconfig

ENV ORACLE_BASE /root/LOCAL_ORACLE/instantclient_11_2
ENV ORACLE_HOME /root/LOCAL_ORACLE/instantclient_11_2
ENV ORACLE_LIB_DIR /root/LOCAL_ORACLE/instantclient_11_2
ENV ORACLE_INC_DIR /root/LOCAL_ORACLE/instantclient_11_2/sdk/include
ENV PATH $PATH:/root/LOCAL_ORACLE/instantclient_11_2
ENV LD_LIBRARY_PATH $LD_LIBRARY_PATH:/root/LOCAL_ORACLE/instantclient_11_2

RUN pip install cx_Oracle
RUN pip install easyaccess --upgrade
